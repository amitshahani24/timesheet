import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment.prod';
import { HttpClient } from '@angular/common/http';
import { TimeSheetResponse, TimeSheetByDay } from '../timesheet/timesheetresponse';

@Injectable()
export class TimeSheetService {
    private baseapi = environment.apiUrl;
    constructor(private http: HttpClient) { }

    getaTimeSheet(employeeId,fromDate,toDate) {
        return this.http.get<TimeSheetResponse>(this.baseapi + "/timesheet?employeeId=" + employeeId
                                                + '&fromDate=' + fromDate + '&toDate=' + toDate);
    }

    saveTimeSheet(timesheet : TimeSheetByDay){
        
       return  this.http.post<TimeSheetByDay>(this.baseapi + "/timesheet",timesheet);
    }
}
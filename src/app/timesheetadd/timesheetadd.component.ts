import { Component, OnInit } from '@angular/core';
import { EmployeeService } from '../services/employee.service';
import { TaskService } from '../services/task.service';
import { TimeSheetByDay } from '../timesheet/timesheetresponse';
import { TimeSheetService } from '../services/timesheet.servicec';
import {ActivatedRoute, Router} from '@angular/router'; 


@Component({
  selector: 'app-timesheetadd',
  templateUrl: './timesheetadd.component.html',
  styleUrls: ['./timesheetadd.component.scss']
})
export class TimesheetaddComponent implements OnInit {

  employees: any;
  selectedEmployee: any;
  tasks:any;
  selectedTask: any;
  timesheet:TimeSheetByDay;
weekstart:string;
weekDates:string[];

  constructor(private employeeService: EmployeeService, private taskService: TaskService, 
    private timeSheetService: TimeSheetService, private route : ActivatedRoute, private router: Router) { 


  }

  ngOnInit() {

    this.timesheet = {} as TimeSheetByDay;
    this.weekstart=this.route.snapshot.params['weekstart'];
    let startDate = new Date(this.weekstart);
  
    this.weekDates =[];
    for(var i=1;i<=7;i++){
        let formattedDate = startDate.toLocaleDateString();
         this.weekDates.push(formattedDate);
         startDate = new Date(startDate);
         startDate.setDate(startDate.getDate() + 1);
    }
    this.timesheet.businessDate = this.weekDates[0];

    this.employeeService.getallemployees().subscribe(data => {
      this.employees = data; 
      this.timesheet.employeeId=  +this.route.snapshot.params['employeeId'];
    
    });

    this.taskService.getallTasks().subscribe(data => {this.tasks=data;});
}

onSubmit = function(){

this.timeSheetService.saveTimeSheet(this.timesheet).subscribe(s => {

  this.router.navigate(['/timesheet',this.timesheet.employeeId]);
}
);
};

onCancel=function(){

  this.router.navigate(['/timesheet',this.timesheet.employeeId]);
};

}



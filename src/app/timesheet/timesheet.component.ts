import { Component, OnInit } from '@angular/core';
import { EmployeeService } from '../services/employee.service';
import { TimeSheetService } from '../services/timesheet.servicec';


import { TimeSheetResponse } from './timesheetresponse';

import * as _ from "lodash";
import { Router, ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-timesheet',
  templateUrl: './timesheet.component.html',
  styleUrls: ['./timesheet.component.scss']
})
export class TimesheetComponent implements OnInit {

  employees: any;
  selectedEmployee: any;
  timesheetResponse:TimeSheetResponse;
  timesheetDates:any;
  weekstart:string;
  weekend :string;

  constructor(private employeeService: EmployeeService, private timesheetService: TimeSheetService,
    private router: Router,private route : ActivatedRoute) { }

  navigateToAddTimeSheet = function(){
    this.router.navigate(['/add-timesheet',this.weekstart,this.selectedEmployee]);

  };

  onChangeEmployee(newEmployee){

   
    this.timesheetService.getaTimeSheet(newEmployee,this.weekstart,this.weekend).subscribe(s => 
      
      {
       
       
        var listTimesheets = _.map(s.timesheetsByTask,'timeSheets');
        listTimesheets = _.flatten(listTimesheets);
        var listbusinessDates = _.map(listTimesheets,'businessDate');
       listbusinessDates =  _.uniq(listbusinessDates)
       listbusinessDates = _.sortBy(listbusinessDates )
        this.timesheetResponse = s;
        this.timesheetDates = listbusinessDates;
       
      });

  }

  

  ngOnInit() {
   
    this.weekstart = '03-04-2019';

    let fromDate = this.weekstart;
    let toDateTemp = new Date(this.weekstart);
    toDateTemp.setDate(toDateTemp.getDate() + 6)
    let toDate = toDateTemp.toLocaleDateString();
    this.weekend = toDate;;

    this.employeeService.getallemployees().subscribe(data => {
      this.employees = data;

      if(this.route.snapshot.params['employeeId']){
        this.selectedEmployee = +this.route.snapshot.params['employeeId'];
        this.onChangeEmployee(this.selectedEmployee);
      }
  });

  }


  goPrevious = function(){

    let fromDateTemp = new Date(this.weekstart);
    fromDateTemp.setDate(fromDateTemp.getDate() - 7);
    let fromDate  = fromDateTemp.toLocaleDateString();
    console.log(fromDate);
    this.weekstart = fromDate;
    console.log(this.weekstart);

    let toDateTemp = new Date(this.weekstart);
    toDateTemp.setDate(toDateTemp.getDate() + 6);
    let toDate = toDateTemp.toLocaleDateString();
   
  
    this.weekend = toDate;


    this. onChangeEmployee(this.selectedEmployee)();
};

goNext = function(){

    let fromDateTemp = new Date(this.weekstart);
    fromDateTemp.setDate(fromDateTemp.getDate() + 7);
    let toDateTemp = new Date(fromDateTemp);
    toDateTemp.setDate(toDateTemp.getDate() + 6);
   
    let fromDate = fromDateTemp.toLocaleDateString();
    let toDate = toDateTemp.toLocaleDateString();
   
    this.weekstart = fromDate;
    this.weekend = toDate;

    this. onChangeEmployee(this.selectedEmployee)();
};

  

}

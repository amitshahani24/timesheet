export interface TimeSheetResponse
{
    employeeId : number,
    timesheetsByTask: TimesheetsByTask[]
}

export interface TimesheetsByTask
{
    taskId : number,
    taskName: string,
    timesheets : TimeSheetByDay[]
}

export interface TimeSheetByDay{
    businessDate:string,
    hours : number,
    taskId:number,
    employeeId:number
}
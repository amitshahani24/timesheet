import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { EmployeeListComponent } from './employee/employee.component';
import { EmployeeService } from './services/employee.service';
import { TimesheetComponent } from './timesheet/timesheet.component';
import {FormsModule} from '@angular/forms';
import { TimeSheetService } from './services/timesheet.servicec';

import { RouterModule, Routes } from '@angular/router';
import { TimesheetaddComponent } from './timesheetadd/timesheetadd.component';
import { TaskService } from './services/task.service';

const appRouts : Routes = [


  { path: 'employee', component: EmployeeListComponent },
  { path: 'timesheet', component: TimesheetComponent },
  { path: 'timesheet/:employeeId', component: TimesheetComponent },
  { path: 'add-timesheet/:weekstart/:employeeId', component: TimesheetaddComponent },
  {path : '', redirectTo:'/employee',pathMatch:'full'}
];

@NgModule({
  declarations: [
    AppComponent,
    EmployeeListComponent,
    TimesheetComponent,
    TimesheetaddComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    RouterModule.forRoot(appRouts),
  
  ],
  providers: [
    EmployeeService,
    TimeSheetService,
    TaskService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

import { Component, OnInit } from '@angular/core';
import { EmployeeService } from '../services/employee.service';

@Component({
    selector: 'employee-list',
    templateUrl: 'employee.component.html',
    styleUrls:['./employee.component.scss']
})

export class EmployeeListComponent implements OnInit {
    employees: any;
    weekstart:string;
    weekend:string;
    constructor(private employeeService: EmployeeService) { }

    ngOnInit() {
this.weekstart = '03-04-2019';


        let fromDate = this.weekstart;
    let toDateTemp = new Date(this.weekstart);
    toDateTemp.setDate(toDateTemp.getDate() + 6)
    let toDate = toDateTemp.toLocaleDateString();

    this.weekend = toDate;

    this.loadEmployeeSummary();
    }

    loadEmployeeSummary = function(){
        this.employeeService.getallemployeeSummmary(this.weekstart,this.weekend).subscribe(data => {
            this.employees = data;
        });
    }

    goPrevious = function(){

        let fromDateTemp = new Date(this.weekstart);
        fromDateTemp.setDate(fromDateTemp.getDate() - 7);
        let fromDate  = fromDateTemp.toLocaleDateString();
        console.log(fromDate);
        this.weekstart = fromDate;
        console.log(this.weekstart);

        let toDateTemp = new Date(this.weekstart);
        toDateTemp.setDate(toDateTemp.getDate() + 6);
        let toDate = toDateTemp.toLocaleDateString();
       
      
        this.weekend = toDate;


        this.loadEmployeeSummary();
    };

    goNext = function(){

        let fromDateTemp = new Date(this.weekstart);
        fromDateTemp.setDate(fromDateTemp.getDate() + 7);
        let toDateTemp = new Date(fromDateTemp);
        toDateTemp.setDate(toDateTemp.getDate() + 6);
       
        let fromDate = fromDateTemp.toLocaleDateString();
        let toDate = toDateTemp.toLocaleDateString();
       
        this.weekstart = fromDate;
        this.weekend = toDate;

        this.loadEmployeeSummary();
    };
}